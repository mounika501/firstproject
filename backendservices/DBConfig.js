const mc=require('mongodb').MongoClient;
const url="mongodb://mouni:mouni@cluster0-shard-00-00-lk0wl.mongodb.net:27017,cluster0-shard-00-01-lk0wl.mongodb.net:27017,cluster0-shard-00-02-lk0wl.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
var dbo;
function initDb()
{
    mc.connect(url,{useNewUrlParser:true},(err,db)=>
    {
        if(err)
        {
            console.log("error in connecting to db");
            console.log(err);
        }
        else{
            console.log("database connected....");
            dbo=db.db("sampledatabase");
        }
    });
}
function getDb()
{
    console.log(dbo,"Db has not been initialised,please called initDb first");
    return dbo;
}
module.exports=
{
    initDb,
    getDb
};