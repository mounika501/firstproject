const exp=require("express");

const app=exp();
const mongo=require("mongodb").MongoClient;
const bodyparser=require("body-parser");
app.use(bodyparser.json());
const url="mongodb+srv://mouni:mouni@cluster0-lk0wl.mongodb.net/test?retryWrites=true&w=majority";
var dbo;
mongo.connect(url,(err,client)=>
{
    if(err)
    {
        console.log(err);

    }
    else
    {
        dbo=client.db("sampledatabase");
        console.log("connected to mongodb");
    }
});
var test=(req,res,next)=>
{
    dbo.collection("samplecollection").find({name:{$eq:req.body.name}}).toArray((err,dataArray)=>
    {
        console.log("length of array is:" +dataArray.length);
        if(err)
        {
            console.log("error in reading user data");
        }
        else if(dataArray.length==0)
        {
            res.json({message:"user not existed"})
        }
        else
        {
            next();
        }
    })
}
module.exports=test;