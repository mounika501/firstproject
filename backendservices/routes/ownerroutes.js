const exp=require('express');
const app=exp();
const ownerroutes=exp.Router();
const bcrypt=require('bcrypt');
const getDb=require('../DBConfig').getDb;
const initDb=require('../DBConfig').initDb;
const checkAuthorisation=require('../middlewares/checkAuthorisation')
// app.use(checkAuthorisation);
const jwt=require('jsonwebtoken');
//token generation
const secretkey="secret";
const nodemailer=require('nodemailer');

initDb();
ownerroutes.get('/ownerprofile',(req,res)=>
{
    dbo=getDb();
    dbo.collection("ownercollection").find().toArray((err,dataArray)=>
    {
        if(err)
        {
            console.log("error in reading data");
        }
        else if(dataArray==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":dataArray});
        }
    })
});


//addhouse get
ownerroutes.get('/addhouse',(req,res)=>
{
    dbo=getDb();
    dbo.collection("housecollection").find().toArray((err,dataArray)=>
    {
        console.log(dataArray)
        if(err)
        {
            console.log("error in reading data");
        }
        else if(dataArray==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":dataArray});

        }
    })
});



// //put view house
// ownerroutes.put('/viewhouse'(req,res)=>{
//     console.log(req.body)
//     var dbo=getDb();
//     dbo.collection('housecollection').updateOne({address:{$eq:req.body.address}},{$set:{status:false}},(err,result)=>{
//         if (err)
//         {
//             console.log(err)
//         }
//         else{
//             dbo.collection('housecollection').find({$and:[{status:{$eq:true}},{ownername:{$eq:req.body.ownername}}]}).toArray((err,dataArray)=>{
//                 if (err)
//                 {
//                     console.log(err)
//                 }
//                 else
//                 {
//                     res.json({message:"successfully deleted",data:dataArray})
//                 }
//             })
//         }
//     })

// })













//get specific house


ownerroutes.post('/viewhouse',checkAuthorisation,(req,res)=>
{
    dbo=getDb();
    
    console.log(req.body);

    dbo.collection('housecollection').find({name:{$eq:req.body[0].name}}).toArray((err,result)=>
    {
        if(err)
        {
            console.log("error in getting house")
        }
        else 
        {
            console.log("result is:",result);
            res.json({message:result});
        }
    })
})









//register post
ownerroutes.post('/register',(req,res,next)=>
{
    console.log(req.body);
    dbo=getDb();

//    if(req.body.name=="" || req.body.passowrd=="" || req.body.dob=="" || req.body.usertype=="" || req.body.emailid=="" || req.body.number=="")
//    {
//        res.json({"message":"please fill all required fields"})
//    }

//    else{

    dbo.collection('ownercollection').find({name:{$eq:req.body.name}}).toArray((err,result)=>{

        if(result=="")
        {
            bcrypt.hash(req.body.password,6,(err,hashpassword)=>{
                if(err)
                {
                    next(err);
                }
                else
                {

                    //mail
                    let transporter=nodemailer.
  createTransport({
      service:"gmail",
      auth:{
          user:"anemounika501@gmail.com",
          pass:"xxxxxxxxxx"
      }
  });
  let info= transporter.sendMail({
      //sender address
      from:'"login details" <anemounika501@gmail.com>',
      //list of recivers
      to:req.body.emailid,
      subject:"owner credentials",//subject line
      text:`name: ${req.body.name},password: ${req.body.password}`,//plain text body
  })



                req.body.password=hashpassword;
                dbo.collection("ownercollection").insertOne(req.body,(err,success)=>
                {

                    if(err)
                    {
                        console.log("error in owner register");
                    }
                    else{
                        res.json({message:"registration success"});
                    }
                })
          
                }
            })
        console.log("unique value");
        }
        else{
            res.json({message:"Duplicate Username"});
            console.log("Duplicate Username");
        }
    })
    
//    }
    })








//post login

ownerroutes.post('/login',(req,res,next)=>
{
    console.log(req.body);
    dbo=getDb();
    if(req.body.usertype=="owner")
    {
    dbo.collection('ownercollection').find({name:{$eq:req.body.name}}).toArray((err,userArray)=>
    {
        console.log("userArray.length is", userArray.length);
       if(userArray.length==0)
        {
            res.json({message:"Invalid owner username"});
        }
       
        else
        {
            bcrypt.compare(req.body.password,userArray[0].password,(err,result)=>
            {
                if(result==true)
                {
                    // ownerdata=userArrray[0].name
                    //create and send token
                    const signedtoken=jwt.sign({name:userArray[0].name},secretkey,{expiresIn:"7d"})
                    console.log(signedtoken);

                    //send signed token as response after successful login


                    res.json({message:"success",data:userArray,token:signedtoken})
                }
                else
                {
                    res.json({message:"Invalid password"})
                }
            })
        }
    })
    }
    else
    {
        dbo.collection('vendorcollection').find({name:{$eq:req.body.name}}).toArray((err,userArray)=>
    {
        console.log(userArray.length);
       if(userArray.length==0)
        {
            res.json({message:"Invalid vendor username"});
        }
       
        else
        {
            bcrypt.compare(req.body.password,userArray[0].password,(err,result)=>
            {
                if(result==true)
                {
                    // vendordata=userArray[0].name
                   //create and send token
                   const signedtoken=jwt.sign({name:userArray[0].name},secretkey,{expiresIn:"7d"})
                   console.log(signedtoken);

                   //send signed token as response after successful login


                   res.json({message:"success",data:userArray,token:signedtoken})

                }
                else
                {
                    res.json({message:"Invalid password"})
                }
            })
        }

    })
    }
    }
)



//post addpayments
ownerroutes.post('/addpayments',checkAuthorisation,(req,res)=>
{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("addpaymentscollection").insertOne(req.body,(err,success)=>
    {
        if(err)
        {
            console.log("error in  addpayments");
        }
        else{
            res.json({message:"add payment methods successfully"});
        }
    })
});


//get specific payments
ownerroutes.post('/viewpayment',checkAuthorisation,(req,res)=>
{
    dbo=getDb();
    
    console.log(req.body);

    dbo.collection('addpaymentscollection').find({name:{$eq:req.body[0].name}}).toArray((err,result)=>
    {
        if(err)
        {
            console.log("error in getting payments")
        }
        else 
        {
            console.log("result is:",result);
            res.json({message:result});
        }
    })
})



//get addpayments

ownerroutes.get('/addpayments',(req,res)=>
{
    dbo=getDb();
    dbo.collection("addpaymentscollection").find().toArray((err,dataArray)=>
    {
        if(err)
        {
            console.log("error in reading data");
        }
        else if(dataArray==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":dataArray});
        }
    })
});




//get data from whomtolet to viewclient
ownerroutes.get('/viewclient/:name',checkAuthorisation,(req,res)=>{
    console.log(req.params)
    var dbo=getDb();
        dbo.collection("whomtolet").find({ownername:{$eq:req.params.name}}).toArray((err,data)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                console.log(data)
                res.json({message:data})
            }
        })
})


//update view client
ownerroutes.put('/viewclient', (req,res)=>{
    console.log(req.body)
    var dbo=getDb();
        dbo.collection("housecollection").updateOne({address:{$eq:req.body.address}},{$set:{reqstatus:req.body.reqstatus,vendorname:req.body.vendorname}},(err,success)=>{
            if(err){
                console.log('error in saving data')
                console.log(err)
            }
            else{
                        res.json({"message": "response sent successfully"})
                    }
        })
})



//owner payment history
ownerroutes.get('/paymenthistory/:name',checkAuthorisation,(req,res)=>{
    var dbo=getDb();
    dbo.collection('vendorpayments').find({ownername:{$eq:req.params.name}}).toArray((err,dataArray)=>{
        if(err){
            console.log(err)
        }
        else
        {
            res.json({data:dataArray})
            
        }
    })
})






// post addhouse
ownerroutes.post('/addhouse',(req,res)=>
{
    
    
    console.log(req.body);
    dbo=getDb();
    dbo.collection("housecollection").insertOne(req.body,(err,success)=>
    {
        if(err)
        {
            console.log("error in add house");
        }
        else{
            res.json({message:"add house successfully"});
        }
    })
});










ownerroutes.put('/update',(req,res,next)=>
{
    console.log(req.body);
    dbo=getDb();
    dbo.collection('ownercollection').update({name:{$eq:req.body.name}},{$set:{number:req.body.number,name:req.body.name,emailid:req.body.emailid}},(err,success)=>
    {
        if(err)
        {
            next(err);

        }
        else
        {
            res.json({message:"update successfully"});
        }
    })
});



ownerroutes.delete('/delete/:ht',(req,res)=>{
    console.log(req.params);
    dbo=getDb();
    dbo.collection('housecollection').deleteOne({ht:{$eq:req.params.ht}},(err,success)=>
    {
        if(err)
        {
            console.log(err);
            console.log("error in delete operation");
        }
        else
        {
            dbo.collection("housecollection").find().toArray((err,dataArray)=>
            {
                if(err)
                {
                    console.log("error in reading");

                }   
                else
                {
                    res.json({message:"Record deleted",
                                data:dataArray
                            })
                }
            })
        }
    })

})






//forgot password
const accountSid = 'ACb909b99c016892e94df57630ed905eaf';
const authToken = '1b6e370a39d8d4fa1058fe88004d4027';
const client = require('twilio')(accountSid, authToken);
ownerroutes.post('/forgotpassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb();
    if(req.body.usertype=='owner'){
        dbname='ownercollection'
    }
    else
    {
        dbname='vendorcollection'
    }
    dbo.collection(dbname).find({name:req.body.name}).toArray((err,userArray)=>{
        if(err){
            next(err)
        }
        else{
            if(userArray.length === 0){
                console.log(userArray.length)
                res.json({"message":"user not found"})
            }
            else{

                jwt.sign({name:userArray[0].name},secretkey,{expiresIn:'7d'},(err,token)=>{
                    if(err){
                     next(err);
                    }
                    else{
                        var OTP=Math.floor(Math.random()*99999)+11111;
                        console.log(OTP)
                        
                        client.messages.create({
                            body: OTP,
                            from: '+12055510730', // From a valid Twilio number
                            to:'+91'+userArray[0].number,  // Text this number
  
                        })
                        .then((message) => {
                            dbo.collection('OTPCollection').insertOne({
                                OTP:OTP,
                                name:userArray[0].name,
                                OTPGeneratedTime:new Date().getTime()+30000
                        },(err,success)=>{
                            if(err){
                                next(err)
                            }
                            else{                                        
                                res.json({"message":"user found",
                                    "token":token,
                                    "OTP":OTP,
                                    "userName":userArray[0].name
                                })
                            }
                        })
                        });

                    }
                    
                })
            }
        }
    })
})

//verify OTP
ownerroutes.post('/verifyotp',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb();
    console.log(new Date().getTime())
    var currentTime=new Date().getTime()
    dbo.collection('OTPCollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('OTPCollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
})

//changing password
ownerroutes.put('/changepassword',(req,res,next)=>{
    console.log(req.body)
    var dbo=getDb();
    if(req.body.usertype=='owner'){
        dbname='ownercollection'
    }
    else
    {
        dbname='vendorcollection'
    }
    bcrypt.hash(req.body.password,5,(err,hashedpassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedpassword)
            dbo.collection(dbname).updateOne({name:req.body.name},{$set:{
                password:hashedpassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
})

//error handling middleware
app.use((err,req,res,next)=>{
    console.log(err);
    })


module.exports=ownerroutes;