const exp=require('express');
const app=exp();

const bodyparser=require("body-parser");
app.use(bodyparser.json());

const adminroutes=require('./routes/adminroutes');
const userroutes=require('./routes/userroutes');
const ownerroutes=require('./routes/ownerroutes');
const vendorroutes=require('./routes/vendorroutes');

app.use("/admin",adminroutes);
app.use("/user",userroutes);
app.use("/owner",ownerroutes);
app.use("/vendor",vendorroutes);

const path=require("path");
console.log(__dirname);
app.use(exp.static(path.join(__dirname,'../dist/mainproject/')));


const port=8080;
app.listen(process.env.PORT || port,()=>console.log(`server running on port ${port}`));