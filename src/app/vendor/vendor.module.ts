import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{FormsModule} from '@angular/forms'
import { VendorRoutingModule } from './vendor-routing.module';
import { VendorviewprofileComponent } from 'src/app/vendorviewprofile/vendorviewprofile.component';
import { VendoraddprofileComponent } from 'src/app/vendoraddprofile/vendoraddprofile.component';



@NgModule({
  declarations: [VendorviewprofileComponent,
    
    VendoraddprofileComponent,],
  imports: [
    CommonModule,
    VendorRoutingModule,
    FormsModule
  ]
})
export class VendorModule { }
