import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 ownerdata:any[];
 vendordata:any[];
 
  constructor(private httpc:HttpClient) { }

  setResponse(data):Observable<any>
{
  return this.httpc.put('/owner/viewclient',data)
}
}
