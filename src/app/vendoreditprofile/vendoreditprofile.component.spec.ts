import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendoreditprofileComponent } from './vendoreditprofile.component';

describe('VendoreditprofileComponent', () => {
  let component: VendoreditprofileComponent;
  let fixture: ComponentFixture<VendoreditprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendoreditprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendoreditprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
