import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerdashboardComponent } from './ownerdashboard/ownerdashboard.component';
import { VendordashboardComponent } from './vendordashboard/vendordashboard.component';
import { OwnerprofileComponent } from './ownerprofile/ownerprofile.component';

import { OwnerpaymentsComponent } from './ownerpayments/ownerpayments.component';
import { VendorpaymentsComponent } from './vendorpayments/vendorpayments.component';
import { VendorprofileComponent } from './vendorprofile/vendorprofile.component';
import { WhomtoletComponent } from './whomtolet/whomtolet.component';
import { OwnerviewprofileComponent } from './ownerviewprofile/ownerviewprofile.component';
import { OwneraddprofileComponent } from './owneraddprofile/owneraddprofile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import { ViewclientComponent } from './viewclient/viewclient.component';
import { LogoutComponent } from './logout/logout.component';
import { ViewpaymentsComponent } from './viewpayments/viewpayments.component';
import { ViewpaymentshisComponent } from './viewpaymentshis/viewpaymentshis.component';
import { AddpaymentsComponent } from './addpayments/addpayments.component';
import { ViewwhouseComponent } from './viewwhouse/viewwhouse.component';
import { VendorviewprofileComponent } from 'src/app/vendorviewprofile/vendorviewprofile.component';
import { VendoraddprofileComponent } from 'src/app/vendoraddprofile/vendoraddprofile.component';
import { DopaymentsComponent } from './dopayments/dopayments.component';
import { VendorpaymenthisComponent } from './vendorpaymenthis/vendorpaymenthis.component';
import { OwnereditprofileComponent } from '../ownereditprofile/ownereditprofile.component';

const routes: Routes = [
{
  path:'ownerdashboard',
  component:OwnerdashboardComponent,
  children:[
    {
      path:'ownerprofile',
      component:OwnerprofileComponent,
      children:[
        {
          path:'ownerviewprofile',
          component:OwnerviewprofileComponent
        },
        {
          path:'ownereditprofile',
          component:OwnereditprofileComponent
        }
       
      ]
    },
   
    {
      path:'ownerpayments',
      component:OwnerpaymentsComponent,
      children:[
        {
          path:'addpayments',
          component:AddpaymentsComponent
        },
        {
          path:'viewpayments',
          component:ViewpaymentsComponent
        },
        {
          path:'viewpaymentshis',
          component:ViewpaymentshisComponent
        }
      ]
    },
    {
      path:'addhouse',
      component:AddhouseComponent
    },
    {
      path:'viewwhouse',
      component:ViewwhouseComponent
    },
    {
      path:'viewclient',
      component:ViewclientComponent
    },
    {path:'logout',component:LogoutComponent}
  ]
},
{
  path:'vendordashboard',
  component:VendordashboardComponent,
  children:[
    {
      path:'vendorprofile',
      component:VendorprofileComponent,
      children:[
        {
          path:'vendorviewprofile',
          component:VendorviewprofileComponent
        },
        {
          path:'vendoraddprofile',
          component:VendoraddprofileComponent
        }
      ]
      
    },
    {
      path:'whomtolet',
      component:WhomtoletComponent
    },
    {
      path:'vendorpayments',
      component:VendorpaymentsComponent,
      children:[
        {
          path:'dopayments',
          component:DopaymentsComponent
        },
        {
          path:'vendorpaymenthis',
          component:VendorpaymenthisComponent

        }
       
      ]
    },
    
  ]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
