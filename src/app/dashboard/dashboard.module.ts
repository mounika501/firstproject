import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { VendordashboardComponent } from './vendordashboard/vendordashboard.component';
import { OwnerdashboardComponent } from './ownerdashboard/ownerdashboard.component';

import { OwnerprofileComponent } from './ownerprofile/ownerprofile.component';


import { OwnerpaymentsComponent } from './ownerpayments/ownerpayments.component';
import { VendorprofileComponent } from './vendorprofile/vendorprofile.component';
import { VendorpaymentsComponent } from './vendorpayments/vendorpayments.component';
import { WhomtoletComponent } from './whomtolet/whomtolet.component';
import { OwnerviewprofileComponent } from './ownerviewprofile/ownerviewprofile.component';
import { OwneraddprofileComponent } from './owneraddprofile/owneraddprofile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import{FormsModule} from '@angular/forms'
import { ViewclientComponent } from './viewclient/viewclient.component';
import { LogoutComponent } from './logout/logout.component';
import { ViewwhouseComponent } from './viewwhouse/viewwhouse.component';
import { AddpaymentsComponent } from './addpayments/addpayments.component';
import { ViewpaymentsComponent } from './viewpayments/viewpayments.component';
import { ViewpaymentshisComponent } from './viewpaymentshis/viewpaymentshis.component';
import { DopaymentsComponent } from './dopayments/dopayments.component';
import { VendorpaymenthisComponent } from './vendorpaymenthis/vendorpaymenthis.component';
import { SearchPipe } from '../search.pipe';

@NgModule({
  declarations: [VendordashboardComponent,   SearchPipe, OwnerdashboardComponent, OwnerprofileComponent,   OwnerpaymentsComponent, VendorprofileComponent, VendorpaymentsComponent, WhomtoletComponent, OwnerviewprofileComponent, OwneraddprofileComponent, AddhouseComponent,  ViewclientComponent, LogoutComponent, ViewwhouseComponent, AddpaymentsComponent, ViewpaymentsComponent, ViewpaymentshisComponent, DopaymentsComponent, VendorpaymenthisComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,FormsModule
  ]
})
export class DashboardModule { }
