import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransferService } from 'src/app/transfer.service';

@Component({
  selector: 'app-addhouse',
  templateUrl: './addhouse.component.html',
  styleUrls: ['./addhouse.component.css']
})
export class AddhouseComponent implements OnInit {

  constructor(private house:HttpClient) { }
  c:object[]=[];

  ngOnInit() {
    
  }
  submitHouseData(val)
  {
    this.c.push(val);
    console.log(this.c);
    this.house.post('owner/addhouse',val).subscribe(res=>
      {
        alert(res['message']);
      })
  }

}
