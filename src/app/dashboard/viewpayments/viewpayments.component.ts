import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewpayments',
  templateUrl: './viewpayments.component.html',
  styleUrls: ['./viewpayments.component.css']
})
export class ViewpaymentsComponent implements OnInit {
data1:object[];
data2:any[];
data5:any[];

  constructor(private ts:TransferService,private httpc:HttpClient,private router:Router) { }

  ngOnInit()
 {
  this.ts.sendPaymentsData().subscribe(data=>
    {
      this.data1=data['message'];
        

    })


//get specific payments

this.data2=this.ts.sendSpecificDataToProfile();
console.log(this.data2);

this.httpc.post('owner/viewpayment',this.data2).subscribe(res=>
  {
    this.data5=res['message'];
    console.log(this.data5);


     //unauthorised access
     if(res['message'] === "unauthorised access")
     {
       alert(res['message'])
       console.log(res["message"])
       this.router.navigate(['/login'])

     }
   else if(res['message'] == "session expired")
     {
       alert(res["message"]);
       this.router.navigate(['/login'])
     }







  })

  }

}
