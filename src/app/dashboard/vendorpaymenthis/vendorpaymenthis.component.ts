import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-vendorpaymenthis',
  templateUrl: './vendorpaymenthis.component.html',
  styleUrls: ['./vendorpaymenthis.component.css']
})
export class VendorpaymenthisComponent implements OnInit {
data:any[];
  constructor(private httpc:HttpClient,private register:RegisterService) { }

  ngOnInit() {

    //get payments
    this.httpc.get(`/vendor/vendorpaymenthistory/${this.register.vendordata[0].name}`).subscribe(res=>{
      this.data=res['data']
      console.log(this.data);
  })

}
}