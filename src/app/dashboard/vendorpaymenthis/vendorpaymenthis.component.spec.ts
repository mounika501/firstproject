import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorpaymenthisComponent } from './vendorpaymenthis.component';

describe('VendorpaymenthisComponent', () => {
  let component: VendorpaymenthisComponent;
  let fixture: ComponentFixture<VendorpaymenthisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorpaymenthisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorpaymenthisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
