import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwneraddprofileComponent } from './owneraddprofile.component';

describe('OwneraddprofileComponent', () => {
  let component: OwneraddprofileComponent;
  let fixture: ComponentFixture<OwneraddprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwneraddprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwneraddprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
