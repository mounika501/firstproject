import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/register.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewpaymentshis',
  templateUrl: './viewpaymentshis.component.html',
  styleUrls: ['./viewpaymentshis.component.css']
})
export class ViewpaymentshisComponent implements OnInit {
data:any[];
  constructor(private register:RegisterService,private httpc:HttpClient,private router:Router) { }

  ngOnInit() {
    this.httpc.get(`/owner/paymenthistory/${this.register.ownerdata[0].name}`).subscribe(
      res=>{
         this.data=res['data']
         console.log(this.data);

  //        //unauthorised access
  //    if(res['message'] === "unauthorised access")
  //    {
  //      alert(res['message'])
  //      console.log(res["message"])
  //      this.router.navigate(['/login'])

  //    }
  //  else if(res['message'] == "session expired")
  //    {
  //      alert(res["message"]);
  //      this.router.navigate(['/login'])
  //    }


      }
    )

  }

}
