import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpaymentshisComponent } from './viewpaymentshis.component';

describe('ViewpaymentshisComponent', () => {
  let component: ViewpaymentshisComponent;
  let fixture: ComponentFixture<ViewpaymentshisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpaymentshisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpaymentshisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
