import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerpaymentsComponent } from './ownerpayments.component';

describe('OwnerpaymentsComponent', () => {
  let component: OwnerpaymentsComponent;
  let fixture: ComponentFixture<OwnerpaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerpaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerpaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
