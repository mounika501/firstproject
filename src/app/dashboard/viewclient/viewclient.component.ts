import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewclient',
  templateUrl: './viewclient.component.html',
  styleUrls: ['./viewclient.component.css']
})
export class ViewclientComponent implements OnInit {
data:any[];
vendordata:any[];
ownerdata:any[];
  constructor(private httpc:HttpClient,private register:RegisterService,private router:Router) { }

  ngOnInit() {


    // this.vendordata=this.register.vendordata;
    // this.ownerdata=this.register.ownerdata;



    //get request from whom to let to view profile
    this.httpc.get(`/owner/viewclient/${this.register.ownerdata[0].name}`).subscribe(res=>{
      this.data=res['message'];

   //unauthorised access
   if(res['message'] === "unauthorised access")
   {
     alert(res['message'])
     console.log(res["message"])
     this.router.navigate(['/login'])

   }
 else if(res['message'] == "session expired")
   {
     alert(res["message"]);
     this.router.navigate(['/login'])
   }



console.log("data for view client is",this.data)
  })

}


accept(data)
{
  data.reqstatus="request accepted";
  this.register.setResponse(data).subscribe(res=>{
    alert(res['message']);
    // this.data=res['data'];
  })
  
}
reject(data)
{
  data.reqstatus="request rejected";
  this.register.setResponse(data).subscribe(res=>{
    alert(res['message'])
    // this.data=res['data'];
  })
}

}
