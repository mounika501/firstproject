import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-dopayments',
  templateUrl: './dopayments.component.html',
  styleUrls: ['./dopayments.component.css']
})
export class DopaymentsComponent implements OnInit {

  constructor(private httpc:HttpClient,private register:RegisterService) { }

  ngOnInit() {
  }
doPayments(data)
{
  console.log(data)
  data.vendorname= this.register.vendordata[0].name
 data.paystatus='payed';
this.httpc.post('/vendor/dopayment',data).subscribe(res=>{
  alert(res['message'])
})
}
}