import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DopaymentsComponent } from './dopayments.component';

describe('DopaymentsComponent', () => {
  let component: DopaymentsComponent;
  let fixture: ComponentFixture<DopaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DopaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DopaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
