import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewwhouse',
  templateUrl: './viewwhouse.component.html',
  styleUrls: ['./viewwhouse.component.css']
})
export class ViewwhouseComponent implements OnInit {
data1:any[];
data:any[];
data2:any[];
data5:any[];
  constructor(private ts:TransferService,private router:Router, private httpc:HttpClient) { }

  ngOnInit() {

    this.ts.sendHouseData().subscribe(data=>
      {
        this.data1=data['message'];
      })

     

     








      //specific house from owner

      this.data2=this.ts.sendSpecificDataToProfile();
      // console.log(this.data2);

      //console.log("name is ",this.data2[0].name)

      this.httpc.post('owner/viewhouse',this.data2).subscribe(res=>
        {
          this.data5=res['message'];
          console.log(this.data5);

           //unauthorised access
      if(res['message'] === "unauthorised access")
      {
        alert(res['message'])
        console.log(res["message"])
        this.router.navigate(['/login'])

      }
    else if(res['message'] == "session expired")
      {
        alert(res["message"]);
        this.router.navigate(['/login'])
      }
        })






  }
  deleteDocument(ht)
  {
    console.log(ht);
    this.httpc.put(`/owner/delete`,ht).subscribe(res=>
      {
        alert(res['message']);
        this.data1=res['data'];
      })
  }

 

}
