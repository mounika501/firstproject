import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewwhouseComponent } from './viewwhouse.component';

describe('ViewwhouseComponent', () => {
  let component: ViewwhouseComponent;
  let fixture: ComponentFixture<ViewwhouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewwhouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewwhouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
