import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerviewprofileComponent } from './ownerviewprofile.component';

describe('OwnerviewprofileComponent', () => {
  let component: OwnerviewprofileComponent;
  let fixture: ComponentFixture<OwnerviewprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerviewprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerviewprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
