import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ownerviewprofile',
  templateUrl: './ownerviewprofile.component.html',
  styleUrls: ['./ownerviewprofile.component.css']
})
export class OwnerviewprofileComponent implements OnInit {
  // z:any[]=[];
  data:any[];
  data2:any[];

 
  constructor(private obj1:TransferService,private http:HttpClient ,private ts:TransferService,private router:Router) { }

  ngOnInit() {
    // this.z=this.obj1.sendDataToOwnerProfile();
    // console.log(this.z);
    this.obj1.sendDataToOwnerProfile().subscribe(data=>
      {
        this.data=data['message'];


        
       
   
      })

      //specific login data

      this.data2=this.ts.sendSpecificDataToProfile();
      console.log(this.data2);



  } 

 b:boolean=false;
 objectToModify:object;
 editProfile(obj)
 {
   this.objectToModify=obj;
   console.log(this.objectToModify);
   
   this.b=true;
 }
 onSubmit(val)
 {
   this.b=false;
   this.http.put('owner/update',val).subscribe(res=>
    {
      alert(res['message']);
       
    })



 }
 

}
