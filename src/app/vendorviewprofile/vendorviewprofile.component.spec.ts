import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorviewprofileComponent } from './vendorviewprofile.component';

describe('VendorviewprofileComponent', () => {
  let component: VendorviewprofileComponent;
  let fixture: ComponentFixture<VendorviewprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorviewprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorviewprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
