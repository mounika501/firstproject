import { Component, OnInit } from '@angular/core';
import { TransferService } from '../transfer.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-vendorviewprofile',
  templateUrl: './vendorviewprofile.component.html',
  styleUrls: ['./vendorviewprofile.component.css']
})
export class VendorviewprofileComponent implements OnInit {
data:any[];
data2:any[];
data4:any[];
  constructor( private ts:TransferService,private http:HttpClient) { }

  ngOnInit() {


    this.ts.sendDataToVendorProfile().subscribe(data=>
      {
        this.data=data['message'];
        console.log("this.data is",this.data);
        this.data4=this.ts.sendSpecificDataToVendorProfile();
        console.log(this.data4);
   
      })
  }


  b:boolean=false;
  objectToModify:object;
  editVendorProfile(obj)
  {
    this.objectToModify=obj;
    console.log(this.objectToModify);
    
    this.b=true;
  }
  onSubmit(val)
  {
    this.b=false;
    this.http.put('vendor/update',val).subscribe(res=>
     {
       alert(res['message']);
     })
    

  }
 

}
