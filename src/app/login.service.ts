import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedin:boolean=false;
 constructor(private hc:HttpClient) { }

 doLogin(ownerObject):Observable<any>
 {
    return this.hc.post<any>('owner/login',ownerObject);
 }
 doLogin1(ownerObject):Observable<any>
 {
    return this.hc.post<any>('owner/login',ownerObject);
 }

 logout()
 {
   localStorage.removeItem("idToken");
   this.isLoggedin=false;
 }
}
