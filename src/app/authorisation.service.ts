import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorisationService implements HttpInterceptor {

  constructor() { }
  intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
  {
    //read token from local storage
    const idToken=localStorage.getItem("idToken");
    
    //if token is found ,it adds it to header of req object
    if(idToken)
    {
      const cloned=req.clone({  
        headers:req.headers.set("Authorization","Bearer " +idToken)
      });
      return next.handle(cloned);
    }
    else
    {
      //if token is not found ,frwd the sm req object
      return next.handle(req);
    }
  }
}
