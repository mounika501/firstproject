import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  c:object[]=[];
  data1:any[];
  data3:any[];
  

  constructor(private hc:HttpClient) { }
  
  receiveDataFromRegister(b)
  {
    this.c=b;
  }
  sendDataToOwnerProfile():Observable<any[]>
  {
    // return this.c;
    return this.hc.get<any[]>('owner/ownerprofile');
  }


  sendDataToVendorProfile():Observable<any[]>
  {
    // return this.c;
    return this.hc.get<any[]>('vendor/vendorprofile');
  }


  sendHouseData():Observable<any[]>
  {
    return this.hc.get<any[]>('owner/addhouse');
  }


  //send payments data
  sendPaymentsData():Observable<any[]>
  {
    return this.hc.get<any[]>('owner/addpayments');
  }


  //specific data from login

  receiveSpecificDataFromLogin(data)
  {
    this.data1=data;
    
  }
  sendSpecificDataToProfile()
  {
    return this.data1;
  }


  //specific data from vendor login
  receiveSpecificDataFromVendorLogin(data)
  {
    this.data3=data;
    
  }
  sendSpecificDataToVendorProfile()
  {
    return this.data3;
  }
}
