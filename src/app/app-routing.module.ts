import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './carousel/carousel.component';



import { AboutusComponent } from './aboutus/aboutus.component';

const routes: Routes = [
 
  {
    path:'',
    redirectTo:'home',
    pathMatch:'full'
      },  
  {
  path:'home',
  component:HomeComponent,
    },
    {
      path:'carousel',
      component:CarouselComponent
    },
   
 
  

{
  path:'aboutus',
  component:AboutusComponent
}


];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
