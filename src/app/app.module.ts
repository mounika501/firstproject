import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './carousel/carousel.component';

import { AboutusComponent } from './aboutus/aboutus.component';

import { OwnerModule } from './owner/owner.module';
import { VendorModule } from './vendor/vendor.module';
import { DashboardModule } from './dashboard/dashboard.module';
import{HttpClientModule, HTTP_INTERCEPTORS}from '@angular/common/http';
import { AuthorisationService } from './authorisation.service';
import { OwnereditprofileComponent } from './ownereditprofile/ownereditprofile.component';
import { VendoreditprofileComponent } from './vendoreditprofile/vendoreditprofile.component';
// import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarouselComponent,
    
    AboutusComponent,
    
    OwnereditprofileComponent,
    
    VendoreditprofileComponent,
    
    // SearchPipe,
    
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwnerModule,VendorModule,
    DashboardModule,HttpClientModule

  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthorisationService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
