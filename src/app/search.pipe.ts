import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(data1: any[], searchTerm: string): any {
    if (!searchTerm)
    {
      return data1;
    }
    else
    {
      return data1.filter(searchItem=>
        searchItem.ht.toLowerCase().indexOf(searchTerm.toLocaleLowerCase())!=-1||
        searchItem.address.toLowerCase().indexOf(searchTerm.toLocaleLowerCase())!=-1||
        searchItem.rent.toLowerCase().indexOf(searchTerm.toLocaleLowerCase())!=-1
      )
    }
  }
  }


