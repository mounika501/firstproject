import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  constructor(private httpc:HttpClient,private router:Router) { }

  ngOnInit() {
  }
otp(val)
{
  this.httpc.post('owner/verifyotp',val).subscribe(res=>{
    alert(res['message']);
    if(res['message']=="verifiedOTP")
    {
      this.router.navigate(['/changepassword'])
    }
    else{
      this.router.navigate(['/otp'])
    }
  })
}
}
