import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private httpc:HttpClient,private router:Router) { }

  ngOnInit() {
  }
changePassword(val)
{
  this.httpc.put('owner/changepassword',val).subscribe(res=>
    {
      alert(res['message'])
      this.router.navigate(['/login'])
    })
}
}
