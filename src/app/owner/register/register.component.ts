import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransferService } from 'src/app/transfer.service';
import{HttpClient}from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  b:object[]=[];
  constructor(private router:Router,private obj1:TransferService,private httpc:HttpClient) { }

  ngOnInit() {
  }
genders:string[]=['Male','Female','Others']

submitRegisterData(val)
  {
    if(val.name=="" || val.emailid=="" || val.password=="" || val.number=="" || val.dob=="" ||val.usertype=="")
    {
      alert("please fill all required fields")
    }
    else{  
       this.b.push(val);
    console.log(this.b);
    
   if(val.usertype=="owner")
   {
    this.httpc.post('owner/register',val).subscribe(res=>
      {

        
        // if(res["message"]=="null values are not allowed")
        // {
        //   alert(res["message"])
        // }


       if(res['message']=="registration success")
        {
          alert("registration success");
          this.router.navigate(['login']);
        }

     





        else if(res['message']=="Duplicate Username"){
          alert("Duplicate Username");
          this.router.navigate(['register']);
        }
      })
      
      this.obj1.receiveDataFromRegister(this.b);
    
  
   }
   else if(val.usertype=="vendor"){
    this.httpc.post('vendor/register',val).subscribe(res=>
      {
        // if(res["message"]=="null values are not allowed")
        // {
        //   alert(res["message"])
        // }
         if(res['message']=="registration success")
        {
          alert("registration success");
          this.router.navigate(['login']);
        }
        else if(res['message']=="Duplicate Username"){
          alert("Duplicate Username");
          this.router.navigate(['register']);
        }
      })
      
      this.obj1.receiveDataFromRegister(this.b);
    
    
    
      // this.router.navigate(['login']);
   }
  }


}
}
