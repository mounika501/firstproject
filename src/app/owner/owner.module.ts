import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{FormsModule} from '@angular/forms';
import { OwnerRoutingModule } from './owner-routing.module';


import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { OtpComponent } from './otp/otp.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgotpasswordComponent, OtpComponent, ChangepasswordComponent],
  imports: [
    CommonModule,
    OwnerRoutingModule,FormsModule
  ]
})
export class OwnerModule { }
