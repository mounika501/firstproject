import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
data:any[];
  constructor(private httpc:HttpClient,private router:Router ) { }

  ngOnInit() {
  }
password(value)
{
  // this.data=value;
  // console.log(this.data);
  this.httpc.post('owner/forgotpassword',value).subscribe(res=>
    {
      alert(res['message']);
      if(res['message']=="user found")
      {
        this.router.navigate(['/otp'])
      }
      else{
        this.router.navigate(['/forgotpassword'])
      }
    })

}
}
