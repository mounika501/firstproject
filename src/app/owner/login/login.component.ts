import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginService } from 'src/app/login.service';
import { TransferService } from 'src/app/transfer.service';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  a:object[]=[];


  data:any[];

  constructor( private obj:Router,private http:HttpClient ,private register:RegisterService,private ls:LoginService,private ts:TransferService) { }

  ngOnInit() {
    this.ls.logout();
     }
  submitLoginData(val)
  {
    this.a=val;
    console.log(this.a);
   
    
    if(val.usertype=="owner")
    {
        console.log("value is",val);
        this.ls.doLogin(val).subscribe(res=>
          {
            this.data=res['data'];
            console.log("this.data is",this.data);
            if(res['message'] === "Invalid owner username")
            {
              alert("Invalid owner username");

            }
            else if(res['message'] == "Invalid password")
            {
              alert("Invalid password");
            }
            else
            {
                alert("owner login success");
                localStorage.setItem("idToken",res['token']);
                this.ls.isLoggedin=true;
                console.log(this.ls.isLoggedin);
              this.obj.navigate(['/ownerdashboard']);



              //register data for one user
              this.register.ownerdata=res['data']

              this.ts.receiveSpecificDataFromLogin(this.data);

              
            }

       
  
     })
    }
 
  
  else
  {


    console.log("value is",val);
    this.ls.doLogin1(val).subscribe(res=>
      {
        this.data=res['data'];
        console.log("this.data is",this.data);
        if(res['message']=="Invalid vendor username")
        {
          alert("Invalid vendor username");

        }
        else if(res['message']=="Invalid password")
        {
          alert("Invalid password");
        }
        else
        {
            alert("vendor login success");
            localStorage.setItem("idToken",res['token']);
          this.obj.navigate(['/vendordashboard']);
          this.ls.isLoggedin=true;
          console.log(this.ls.isLoggedin);
          this.ts.receiveSpecificDataFromVendorLogin(this.data);


          //register data for one vendor
          this.register.vendordata=res['data']

   
          
        }

      })


  }
}
  }