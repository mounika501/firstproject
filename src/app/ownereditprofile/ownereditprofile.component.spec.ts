import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnereditprofileComponent } from './ownereditprofile.component';

describe('OwnereditprofileComponent', () => {
  let component: OwnereditprofileComponent;
  let fixture: ComponentFixture<OwnereditprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnereditprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnereditprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
