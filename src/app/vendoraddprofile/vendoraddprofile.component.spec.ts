import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendoraddprofileComponent } from './vendoraddprofile.component';

describe('VendoraddprofileComponent', () => {
  let component: VendoraddprofileComponent;
  let fixture: ComponentFixture<VendoraddprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendoraddprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendoraddprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
